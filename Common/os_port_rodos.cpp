#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <new>

#include "os_port_rodos.h"
#include "os_port.h"

#include "debug.h"
#include "net_config.h"
#include "core/net.h"

#include "event.h"
#include "misc-rodos-funcs.h"
#include "rodos-debug.h"
#include "rodos-semaphore.h"
#include "thread.h"
#include "timemodel.h"
#include "yprintf.h"

/******************************************************************************
 * debug output
 ******************************************************************************/
namespace RODOS {
   extern Semaphore printfProtector;
   extern size_t vsnprintf(char *dest, size_t size, const char *fmt, va_list args);
}

extern "C" {
void osPrintf_impl(const char *fmt, ...) {
   RODOS::Yprintf yprintf;
   va_start(yprintf.ap, fmt);
   if (!RODOS::isSchedulerRunning()) {
      yprintf.vaprintf(fmt);
   } else {
      RODOS::printfProtector.enter();
      yprintf.vaprintf(fmt);
      RODOS::printfProtector.leave();
   }
   RODOS::FFLUSH();
}
int osSprintf_impl(char* dest, const char* fmt, ...) {
   va_list args;
   va_start(args, fmt);
   size_t chars_written = RODOS::vsnprintf(dest, SPRINTF_MAX_SIZE, fmt, args);
   va_end(args);
   return static_cast<int>(chars_written);
}
int osSnprintf_impl(char* dest, size_t size, const char* fmt, ...) {
   va_list args;
   va_start(args, fmt);
   size_t chars_written = RODOS::vsnprintf(dest, size, fmt, args);
   va_end(args);
   return static_cast<int>(chars_written);
}
} /* extern "C" */



/******************************************************************************
 * task / thread management
 ******************************************************************************/

// RODOS uses static memory allocation wherever possible. But unfortunately
// it's not completely compatible with Cyclone's (static) task implementation.
// => As workaround we (pre-)define each task beforehand and
//    exchange their "payload" later (@see osCreateTask(...))

template <size_t STACK_SIZE>
class CycloneTrd final : public RODOS::StaticThread<STACK_SIZE> {
public:
   CycloneTrd(const char *name, const int32_t priority)
      : RODOS::StaticThread<STACK_SIZE>(name, priority),
        task_(nullptr), param_(nullptr) { }

   bool setPayload(OsTaskCode task, void *param) {
      if (this->task_ == nullptr) {
         this->param_ = param;
         this->task_ = task;
         if (netContext.running) this->resume();
         return true;
      }
      return false;
   }

private:
   void run() {
      while (true) {
         while (this->task_ == nullptr)
            RODOS::Thread::suspendCallerUntil(RODOS::END_OF_TIME);
         this->task_(this->param_);

         this->param_ = nullptr;
         this->task_ = nullptr;
      }
   }

   OsTaskCode task_;
   void *param_;
};

#include "core/net.h"
static CycloneTrd<NET_TASK_STACK_SIZE> g_trd_net("[Cyclone] TCP/IP Stack", NET_TASK_PRIORITY);

#if (COAP_SERVER_SUPPORT == ENABLED)
#include "coap/coap_server.h"
static CycloneTrd<COAP_SERVER_STACK_SIZE> g_trd_coap_server("[Cyclone] CoAP Server", COAP_SERVER_PRIORITY);
#endif

#if (IPV6_SUPPORT == ENABLED && DHCPV6_RELAY_SUPPORT == ENABLED)
#include "dhcpv6/dhcpv6_relay.h"
static CycloneTrd<DHCPV6_RELAY_STACK_SIZE> g_trd_dhcpv6_relay("[Cyclone] DHCPv6 Relay", DHCPV6_RELAY_PRIORITY);
#endif

#if (ECHO_SERVER_SUPPORT == ENABLED)
#include "echo/echo_server.h"
static CycloneTrd<ECHO_SERVER_STACK_SIZE> g_trd_echo_server("[Cyclone] Echo Server", ECHO_SERVER_PRIORITY);
#endif

#if (FTP_SERVER_SUPPORT == ENABLED)
#include "ftp/ftp_server.h"
static CycloneTrd<FTP_SERVER_STACK_SIZE> g_trd_ftp_server("[Cyclone] FTP Server", FTP_SERVER_PRIORITY);
#endif

#if (HTTP_SERVER_SUPPORT == ENABLED)
#error "HTTP-Server support not functional"
// Problem: the HTTP-Server implementation needs a variable amount of connection threads
#endif

#if (ICECAST_CLIENT_SUPPORT == ENABLED)
#include "icecast/icecast_client.h"
static CycloneTrd<ICECAST_CLIENT_STACK_SIZE> g_trd_icecast_client("[Cyclone] Icecast client", ICECAST_CLIENT_PRIORITY);
#endif

#if (LLDP_SUPPORT == ENABLED)
#include "lldp/lldp.h"
static CycloneTrd<LLDP_TASK_STACK_SIZE> g_trd_lldp("[Cyclone] LLDP Agent", LLDP_TASK_PRIORITY);
#endif

#if (MODBUS_SERVER_SUPPORT == ENABLED)
#include "modbus/modbus_server.h"
static CycloneTrd<MODBUS_SERVER_STACK_SIZE> g_trd_modbus_server("[Cyclone] Modbus/TCP Server", MODBUS_SERVER_PRIORITY);
#endif

#if (SNMP_AGENT_SUPPORT == ENABLED)
#include "snmp/snmp_agent.h"
static CycloneTrd<SNMP_AGENT_STACK_SIZE> g_trd_snmp("[Cyclone] SNMP Agent", SNMP_AGENT_PRIORITY);
#endif

#if (TFTP_SERVER_SUPPORT == ENABLED)
#include "tftp/tftp_server.h"
static CycloneTrd<TFTP_SERVER_STACK_SIZE> g_trd_tftp_server("[Cyclone] TFTP Server", TFTP_SERVER_PRIORITY);
#endif

extern "C" {
const OsTaskParameters OS_TASK_DEFAULT_PARAMS =
{
   DEFAULT_STACKSIZE, //Size of the stack
   DEFAULT_THREAD_PRIORITY //Task priority
};

void osInitKernel(void) { }
void osStartKernel(void) {
   g_trd_net.resume();

#if (COAP_SERVER_SUPPORT == ENABLED)
   g_trd_coap_server.resume();
#endif

#if (IPV6_SUPPORT == ENABLED && DHCPV6_RELAY_SUPPORT == ENABLED)
   g_trd_dhcpv6_relay.resume();
#endif

#if (ECHO_SERVER_SUPPORT == ENABLED)
   g_trd_echo_server.resume();
#endif

#if (FTP_SERVER_SUPPORT == ENABLED)
   g_trd_ftp_server.resume();
#endif

#if (HTTP_SERVER_SUPPORT == ENABLED)
#error "HTTP-Server support not functional"
#endif

#if (ICECAST_CLIENT_SUPPORT == ENABLED)
   g_trd_icecast_client.resume();
#endif

#if (LLDP_SUPPORT == ENABLED)
   g_trd_lldp.resume();
#endif

#if (MODBUS_SERVER_SUPPORT == ENABLED)
   g_trd_modbus_server.resume();
#endif

#if (SNMP_AGENT_SUPPORT == ENABLED)
   g_trd_snmp.resume();
#endif

#if (TFTP_SERVER_SUPPORT == ENABLED)
   g_trd_tftp_server.resume();
#endif
}

OsTaskId osCreateTask(const char_t *name, OsTaskCode taskCode, void *arg,
   const OsTaskParameters *params) {
   static_cast<void>(name);
   static_cast<void>(params);

   RODOS::Thread *trd = nullptr;
   if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&netTaskEx)) {
      if (g_trd_net.setPayload(taskCode, arg))
         trd = &g_trd_net;
   }
#if (COAP_SERVER_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&coapServerTask)) {
      if (g_trd_coap_server.setPayload(taskCode, arg))
         trd = &g_trd_coap_server;
   }
#endif
#if (IPV6_SUPPORT == ENABLED && DHCPV6_RELAY_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&dhcpv6RelayTask)) {
      if (g_trd_dhcpv6_relay.setPayload(taskCode, arg))
         trd = &g_trd_dhcpv6_relay;
   }
#endif
#if (ECHO_SERVER_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&echoServerTask)) {
      if (g_trd_echo_server.setPayload(taskCode, arg))
         trd = &g_trd_echo_server;
   }
#endif
#if (FTP_SERVER_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&ftpServerTask)) {
      if (g_trd_ftp_server.setPayload(taskCode, arg))
         trd = &g_trd_ftp_server;
   }
#endif
#if (ICECAST_CLIENT_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&icecastClientTask)) {
      if (g_trd_icecast_client.setPayload(taskCode, arg))
         trd = &g_trd_icecast_client;
   }
#endif
#if (LLDP_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&lldpTask)) {
      if (g_trd_lldp.setPayload(taskCode, arg))
         trd = &g_trd_lldp;
   }
#endif
#if (MODBUS_SERVER_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&modbusServerTask)) {
      if (g_trd_modbus_server.setPayload(taskCode, arg))
         trd = &g_trd_modbus_server;
   }
#endif
#if (SNMP_AGENT_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&snmpAgentTask)) {
      if (g_trd_snmp.setPayload(taskCode, arg))
         trd = &g_trd_snmp;
   }
#endif
#if (TFTP_SERVER_SUPPORT == ENABLED)
   else if (reinterpret_cast<uintptr_t>(taskCode) == reinterpret_cast<uintptr_t>(&tftpServerTask)) {
      if (g_trd_tftp_server.setPayload(taskCode, arg))
         trd = &g_trd_tftp_server;
   }
#endif
   if (trd != nullptr) {
      if (trd->getStackSize() != params->stackSize) {
         TRACE_WARNING("Stack size of task \"%s\" differs from default value!\n", trd->getName());
         TRACE_WARNING("PROBLEM: RODOS's stacks are fixed at compile time => try changing the default value\n", trd->getName());
      }
      trd->setPriority(params->priority);
      return trd->listElementID;
   } else {
      return OS_INVALID_TASK_ID;
   }
}

void osDelayTask(systime_t delay) {
   RODOS::Thread::suspendCallerUntil(RODOS::NOW() + delay * RODOS::MILLISECONDS);
}
void osSwitchTask() {
   RODOS::Thread::yield();
}
OsTaskId osSelfTaskId(void) {
   return RODOS::Thread::getCurrentThread()->listElementID;
}
} /* extern "C" */


/******************************************************************************
 * event management
 ******************************************************************************/

static_assert(alignof(RODOS::Event) == 4,
   "alignment of \"RODOS::Event\" isn't as expected => check the definition of \"OsEvent\"");
static_assert(sizeof(RODOS::Event) == 8,
   "size of \"RODOS::Event\" isn't as expected => check the definition of \"OsEvent\"");

extern "C" {
bool_t osCreateEvent(OsEvent *event) {
   RODOS::Event *evt = new(event->buff) RODOS::Event();
   return (reinterpret_cast<uintptr_t>(evt) == reinterpret_cast<uintptr_t>(event->buff));
}
void osDeleteEvent(OsEvent *event) {
   reinterpret_cast<RODOS::Event*>(event->buff)->~Event();
}
void osSetEvent(OsEvent *event) {
   reinterpret_cast<RODOS::Event*>(event->buff)->set();
}
void osResetEvent(OsEvent *event) {
   reinterpret_cast<RODOS::Event*>(event->buff)->reset();
}
bool_t osWaitForEvent(OsEvent *event, systime_t timeout) {
   int64_t wait_until;
   if (timeout == INFINITE_DELAY)
      wait_until = RODOS::END_OF_TIME;
   else
      wait_until = RODOS::NOW() + timeout * RODOS::MILLISECONDS;

   RODOS::Event* evt = reinterpret_cast<RODOS::Event*>(event->buff);
   if (evt->suspendUntilTriggered(wait_until)) {
      evt->reset();
      return true;
   } else {
      return false;
   }
}
bool_t osSetEventFromIsr(OsEvent *event) {
   return reinterpret_cast<RODOS::Event*>(event->buff)->set();
}
} /* extern "C" */



/******************************************************************************
 * semaphore and mutex
 ******************************************************************************/

static_assert(alignof(RODOS::Semaphore) == 4,
   "alignment of \"RODOS::Semaphore\" isn't as expected => check the definition of \"OsSemaphore\"");
static_assert(sizeof(RODOS::Semaphore) == 16,
   "size of \"RODOS::Semaphore\" isn't as expected => check the definition of \"OsSemaphore\"");

extern "C" {
bool_t osCreateSemaphore(OsSemaphore *semaphore, uint_t count) {
   if (count != 1) {
      TRACE_WARNING("Counting semaphores not supported => default to 1\n");
      count = 1;
   }
   RODOS::Semaphore *sem = new(semaphore->buff) RODOS::Semaphore();
   return (reinterpret_cast<uintptr_t>(sem) == reinterpret_cast<uintptr_t>(semaphore->buff));
}

void osDeleteSemaphore(OsSemaphore *semaphore) {
   static_cast<void>(semaphore);
}

bool_t osWaitForSemaphore(OsSemaphore *semaphore, systime_t timeout) {
   if (timeout != INFINITE_DELAY)
      TRACE_WARNING("Timeout values other than \"INFINITE_DELAY\" not supported\n");

   reinterpret_cast<RODOS::Semaphore*>(semaphore->buff)->enter();
   return true;
}

void osReleaseSemaphore(OsSemaphore *semaphore) {
   reinterpret_cast<RODOS::Semaphore*>(semaphore->buff)->leave();
}



bool_t osCreateMutex(OsMutex *mutex) {
   RODOS::Semaphore *sem = new(mutex->buff) RODOS::Semaphore();
   return (reinterpret_cast<uintptr_t>(sem) == reinterpret_cast<uintptr_t>(mutex->buff));
}

void osDeleteMutex(OsMutex *mutex) {
   static_cast<void>(mutex);
}

void osAcquireMutex(OsMutex *mutex) {
   reinterpret_cast<RODOS::Semaphore*>(mutex->buff)->enter();
}

void osReleaseMutex(OsMutex *mutex) {
   reinterpret_cast<RODOS::Semaphore*>(mutex->buff)->leave();
}
} /* extern "C" */



/******************************************************************************
 * misc
 ******************************************************************************/
extern "C" {

systime_t osGetSystemTime() {
   return static_cast<systime_t>(RODOS::NOW() / RODOS::MILLISECONDS);
}

// TODO: rework with proper heap management
static RODOS::Semaphore g_heap_sem;
void *osAllocMem(size_t size) {
   g_heap_sem.enter();
   void *mem = malloc(size);
   g_heap_sem.leave();

   if (mem == nullptr)
      TRACE_WARNING("OUT OF HEAP!\n");

   return mem;
}
void osFreeMem(void *p) {
   g_heap_sem.enter();
   free(p);
   g_heap_sem.leave();
}

} /* extern "C" */

#ifndef _OS_PORT_RODOS_H
#define _OS_PORT_RODOS_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdalign.h>

#include "compiler_port.h"

#include <default-platform-parameter.h>

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((__format__(__printf__, 1, 2)))
void osPrintf_impl(const char *fmt, ...);

__attribute__((__format__(__printf__, 2, 3)))
int osSprintf_impl(char* dest, const char *fmt, ...);

__attribute__((__format__(__printf__, 3, 4)))
int osSnprintf_impl(char* dest, size_t size, const char *fmt, ...);

#define osPrintf(...) osPrintf_impl(__VA_ARGS__)
#define osSprintf(dest, ...) osSprintf_impl(dest, __VA_ARGS__)
#define osSnprintf(dest, size, ...) osSnprintf_impl(dest, size, __VA_ARGS__)

#define TRACE_PRINTF(...) osPrintf_impl(__VA_ARGS__)
#define TRACE_ARRAY(...)
#define TRACE_MPI(...)

#define OS_INVALID_TASK_ID -1
#define OS_SELF_TASK_ID osSelfTaskId()

#ifndef OS_TASK_PRIORITY_NORMAL
   #define OS_TASK_PRIORITY_NORMAL (DEFAULT_THREAD_PRIORITY)
#endif

#ifndef OS_TASK_PRIORITY_HIGH
   #define OS_TASK_PRIORITY_HIGH (DEFAULT_THREAD_PRIORITY * 2)
#endif

#ifndef OS_MS_TO_SYSTICKS
   #define OS_MS_TO_SYSTICKS(n) (n)
#endif

#ifndef OS_SYSTICKS_TO_MS
   #define OS_SYSTICKS_TO_MS(n) (n)
#endif

#ifndef osGetSystemTime64
   #define osGetSystemTime64() osGetSystemTime()
#endif

#ifndef osEnterTask
   #define osEnterTask()
#endif

#ifndef osExitTask
   #define osExitTask()
#endif

#ifndef osEnterIsr
   #define osEnterIsr()
#endif

#ifndef osExitIsr
   extern void __asmSaveContextAndCallScheduler(void);
   #define osExitIsr(flag) if (flag) __asmSaveContextAndCallScheduler()
#endif



/**
 * @brief System time (in milliseconds)
 **/
typedef uint32_t systime_t;

/**
 * @brief Task identifier
 **/
typedef int32_t OsTaskId;

/**
 * @brief Task parameters
 **/
typedef struct
{
   size_t stackSize;
   int32_t priority;
} OsTaskParameters;

/**
 * @brief Event object
 **/
typedef struct {
   /**
    * @note An object of type `RODOS::Event` will be placed inside
    */
   __attribute__ ((aligned (4))) uint8_t buff[8];
} OsEvent;

/**
 * @brief Semaphore object
 **/
typedef struct {
   /**
    * @note An object of type `RODOS::Semaphore` will be placed inside
    */
   __attribute__ ((aligned (4))) uint8_t buff[16];
} OsSemaphore;

/**
 * @brief Mutex object
 **/
typedef struct {
   /**
    * @note An object of type `RODOS::Semaphore` will be placed inside
    */
   __attribute__ ((aligned (4))) uint8_t buff[16];
} OsMutex;

/**
 * @brief Task routine
 **/
typedef void (*OsTaskCode)(void *param);


//Default task parameters
extern const OsTaskParameters OS_TASK_DEFAULT_PARAMS;

void osInitKernel(void);
void osStartKernel(void);

/**
 * @brief Creates a new task in the system
 * @warning RODOS doesn't support dynamic Task generation!
 *    Each Task must therefore be pregenerated (@see `osCreateTask(...)` in os_port_rodos.cpp)
 **/
OsTaskId osCreateTask(const char_t *name, OsTaskCode taskCode, void *arg,
   const OsTaskParameters *params);

/**
 * @brief Stops the task from beeing scheduled
 * @warning Two assumptions were made:
 *    1) Tasks only kill themself
 *    2) Is called from within tasks upmost function
 **/
#define osDeleteTask(_param_) return
void osDelayTask(systime_t delay);
void osSwitchTask(void);
OsTaskId osSelfTaskId(void);
//void osSuspendAllTasks(void); // not implemented
//void osResumeAllTasks(void); // not implemented

//Event management
bool_t osCreateEvent(OsEvent *event);
void osDeleteEvent(OsEvent *event);
void osSetEvent(OsEvent *event);
void osResetEvent(OsEvent *event);
bool_t osWaitForEvent(OsEvent *event, systime_t timeout);
bool_t osSetEventFromIsr(OsEvent *event);

//Semaphore management
bool_t osCreateSemaphore(OsSemaphore *semaphore, uint_t count);
void osDeleteSemaphore(OsSemaphore *semaphore);
bool_t osWaitForSemaphore(OsSemaphore *semaphore, systime_t timeout);
void osReleaseSemaphore(OsSemaphore *semaphore);

//Mutex management
bool_t osCreateMutex(OsMutex *mutex);
void osDeleteMutex(OsMutex *mutex);
void osAcquireMutex(OsMutex *mutex);
void osReleaseMutex(OsMutex *mutex);

//System time
systime_t osGetSystemTime(void);

//Memory management
void *osAllocMem(size_t size);
void osFreeMem(void *p);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _OS_PORT_RODOS_H */

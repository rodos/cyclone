# Cyclone - TCP/IP stack

## IMPORTANT

This repo is a **fork** of the Cyclone TCP/IP stack from ORYX EMBEDDED.
The original sources are available on their Website:

[https://www.oryx-embedded.com](https://www.oryx-embedded.com)

and also on GitHub:

[https://github.com/Oryx-Embedded](https://github.com/Oryx-Embedded)

## License

GPL-2.0